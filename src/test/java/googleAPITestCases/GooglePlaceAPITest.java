package googleAPITestCases;
import org.testng.annotations.Test;

import Utilities.ReusableMethods;
import base.TestBase;
import org.apache.logging.log4j.*;
import static org.hamcrest.Matchers.equalTo;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import googleAPI.Payload;
import googleAPI.Resources;
import static io.restassured.RestAssured.given;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;

public class GooglePlaceAPITest extends TestBase {
	private static Logger log = LogManager.getLogger(GooglePlaceAPITest.class.getName());
	
	@Test
	public void getPlace() {
		log.info("HOST NAME" +prop.getProperty("HOST"));
		RestAssured.baseURI = prop.getProperty("HOST");
		given().
		param("key",prop.get("KEY")).
		param("location",prop.getProperty("LOCATION")).
		param("radius",prop.getProperty("RADIUS")).
		when().
		get(Resources.getPlaceResource()).then().assertThat().statusCode(200).
		and().contentType(ContentType.JSON).and().body("results[0].name",equalTo("Sydney"))
		.and().
		body("results[0].place_id",equalTo("ChIJP3Sa8ziYEmsRUKgyFmh9AQM")).and()
		.header("server","scaffolding on HTTPServer2");	
	}
	
// we can pass these in the request
    /*header("dfd","fsdfds").
    cookie("dsfs","csder").
    body()*/
	
	// so we can check status code of response, body, content type , header
	
	
	@Test
	public void addPlaceAPI() {
		log.info("HOST NAME" +prop.getProperty("HOST2"));

		RestAssured.baseURI = prop.getProperty("HOST2");
		given().
		queryParam("key",prop.get("KEY2")).
		body(Payload.addPlace()).when().
		post(Resources.addPlaceResource()).then().assertThat().statusCode(200).and().contentType(ContentType.JSON).and().
		body("status",equalTo("OK"));
	}
	
	@Test
	public void addAndDeletePlace() {
		RestAssured.baseURI = prop.getProperty("HOST2");
		Response res =given().
		queryParam("key",prop.get("KEY2")).
		body(Payload.addPlace()).when().
		post(Resources.addPlaceResource()).then().assertThat().statusCode(200).and().contentType(ContentType.JSON).and().
		body("status",equalTo("OK")).extract().response();
		
		 // here the response is raw so convert it to String 
		String responseString = res.asString();
	//	System.out.println(responseString);
		JsonPath js = new JsonPath(responseString);
		String placeId = js.get("place_id");  // grab Place id
		
		given().
		queryParam("key",prop.getProperty("KEY2")).		
		body("{"+
  "\"place_id\": \""+placeId+"\""+       // pass placeid here in the payload
"}").
		when().
		post(Resources.deletePlaceResource()).
		then().assertThat().statusCode(200).and().contentType(ContentType.JSON).and().
		body("status",equalTo("OK"));
	}
	
	@Test
	public void postPlaceXML() throws IOException {
		String postData = ReusableMethods.GenerateStringFromResource(System.getProperty("user.dir")+"/XMLPayloads/addPlaceXML.xml");
		RestAssured.baseURI = prop.getProperty("HOST2");
		Response res= given().
		queryParam("key",prop.get("KEY2")).
		body(postData).when().
		post(Resources.addPlaceResourceXML()).then().assertThat().statusCode(200).and().contentType(ContentType.XML)
		.extract().response();
		XmlPath x = ReusableMethods.rawToXML(res);
		
	//	System.out.println("Response XML is" + responseXML);
	//	System.out.println("Place id is" + x.get("response.place_id"));		
	}
	
	@Test
	public void extractingNames() {
		RestAssured.baseURI = prop.getProperty("HOST");
		Response res = given().
		param("key",prop.get("KEY")).
		param("location",prop.getProperty("LOCATION")).
		param("radius",prop.getProperty("RADIUS")).log().all().
		when().
		get(Resources.getPlaceResource()).then().assertThat().statusCode(200).
		and().contentType(ContentType.JSON).and().body("results[0].name",equalTo("Sydney"))
		.and().
		body("results[0].place_id",equalTo("ChIJP3Sa8ziYEmsRUKgyFmh9AQM")).and()
		.header("server","scaffolding on HTTPServer2").log().body().
		extract().response();
		JsonPath js =ReusableMethods.rawToJson(res);
		int count = js.get("results.size()");
		for(int i=0; i<count;i++) {
	//	System.out.println(js.get("results[" + i + "].name"));	
		}		
	}	
}
