package libraryAPI;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.apache.logging.log4j.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Utilities.ReusableMethods;
import base.TestBase;
import googleAPITestCases.GooglePlaceAPITest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import libraryBook.Resources;
import libraryBook.bookPayload;

public class BookLibraryTest extends TestBase{
	private static Logger log = LogManager.getLogger(BookLibraryTest.class.getName());
	
	//Dynamic Payload with 1 dataset
	@Test
	public void addAndDeleteOneBook() {
	log.info("HOST NAME" +prop.getProperty("LIBHOST"));
	RestAssured.baseURI = prop.getProperty("LIBHOST");
	System.out.println("prop.getProperty(\"LIBHOST\")" + prop.getProperty("LIBHOST"));
	Response res = given().
	header("Content-Type","application/json").
	body(bookPayload.addBook("affffefre","64562")).when().
	post(Resources.addBookResource()).then().assertThat().statusCode(200).and().contentType(ContentType.JSON)
	.extract().response();
	JsonPath js = ReusableMethods.rawToJson(res);
	String addedId = js.get("ID");
//	System.out.println(addedId);
	//Delete the book
	given().header("Content-Type","application/json").body(bookPayload.deleteBook(addedId)).
	when().post(Resources.deleteBookResource()).then().assertThat().statusCode(200).and().
	contentType(ContentType.JSON).and().body("msg",equalTo("book is successfully deleted"));
	}
	
	//Dynamic payload with multiple dataset
	@Test(dataProvider = "BooksData")	
	public void addBooksAndDelete(String isbn, String aisle ) {
		log.info("HOST NAME" +prop.getProperty("LIBHOST"));
		RestAssured.baseURI = prop.getProperty("LIBHOST");
		Response res = given().
		header("Content-Type","application/json").
		body(bookPayload.addBook(isbn,aisle)).when().
		post(Resources.addBookResource()).then().assertThat().statusCode(200).and().contentType(ContentType.JSON)
		.extract().response();
		JsonPath js = ReusableMethods.rawToJson(res);
		String addedId = js.get("ID");
//		System.out.println(addedId);	
		
		//Delete the books
		given().header("Content-Type","application/json").body(bookPayload.deleteBook(addedId)).
		when().post(Resources.deleteBookResource()).then().assertThat().statusCode(200).and().
		contentType(ContentType.JSON).and().body("msg",equalTo("book is successfully deleted"));
			
	}
	@DataProvider (name = "BooksData")
	public Object[][] getMultipleDataset() {	
		return new Object[][] {{"dgrhgh","76247"},{"fhdhhggh","7672599"},{"ftcfvj","4356354"}};
	}
	
}
