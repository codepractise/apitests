package twitterAPI;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import Utilities.ReusableMethods;

import static  io.restassured.RestAssured.given;
import base.TestBase;
import googleAPITestCases.GooglePlaceAPITest;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class TwitterAPITest extends TestBase{
	private static Logger log = LogManager.getLogger(TwitterAPITest.class.getName());
	protected String id;
	
	@Test
	public void getTweet() {
		log.info("HOST NAME" +prop.getProperty("TWITTER"));
		RestAssured.baseURI = prop.getProperty("TWITTER");
		Response res= given().auth().oauth(prop.getProperty("ConsumerKey"), 
				prop.getProperty("ConsumerSecret"), prop.getProperty("AccessToken"), 
				prop.getProperty("TokenSecret")).queryParam("count","1").when().
		get(Resources.getTweetResource()).then().statusCode(200).extract().response();
		JsonPath js = ReusableMethods.rawToJson(res);
	//	System.out.println(js.get("id"));
	}
	
	@Test
	public String createTweet() {
		log.info("HOST NAME" +prop.getProperty("TWITTER"));
		RestAssured.baseURI = prop.getProperty("TWITTER");
		Response res = given().auth().oauth(prop.getProperty("ConsumerKey"), 
				prop.getProperty("ConsumerSecret"), prop.getProperty("AccessToken"), 
				prop.getProperty("TokenSecret")).queryParam("count","1").
		queryParam("status", "I am tweeting from Postman" + ReusableMethods.generateRandomNumber()).
		when().
		post(Resources.createTweetResource()).then().statusCode(200).extract().response();
		JsonPath js = ReusableMethods.rawToJson(res);
//		System.out.println(js.get("text"));
		id = js.get("id").toString();	
		return id;
	}
	
	@Test
	public void deleteTweet() {
		log.info("HOST NAME" +prop.getProperty("TWITTER"));
		RestAssured.baseURI = prop.getProperty("TWITTER");
		Response res = given().auth().oauth(prop.getProperty("ConsumerKey"), 
				prop.getProperty("ConsumerSecret"), prop.getProperty("AccessToken"), 
				prop.getProperty("TokenSecret")).queryParam("count","1").
		queryParam("status", "I am tweeting from Postman" + ReusableMethods.generateRandomNumber()).
		when().
		post("destroy/" + createTweet() +".json").then().statusCode(200).extract().response();
		JsonPath js = ReusableMethods.rawToJson(res);
		Assert.assertEquals(js.get("truncated"), false);
		Assert.assertEquals(js.get("id").toString(),id);	
	}
}
