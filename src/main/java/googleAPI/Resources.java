package googleAPI;

public class Resources {
	private static String resource;

	public static String getPlaceResource() {
		resource = "/maps/api/place/nearbysearch/json";
		return resource;
	}

	public static String addPlaceResource() {
		resource = "/maps/api/place/add/json";
		return resource;
	}

	public static String deletePlaceResource() {
		resource = "/maps/api/place/delete/json";
		return resource;
	}
	
	public static String addPlaceResourceXML() {
		resource = "/maps/api/place/add/xml";
		return resource;
	}

}
