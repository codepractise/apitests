package base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

public class TestBase {
	protected Properties prop = new Properties();
	
	@BeforeMethod
	public void setUp() throws IOException {
		FileInputStream ip = new FileInputStream(System.getProperty("user.dir")+"/config.properties");
		prop.load(ip);	
	}
}
